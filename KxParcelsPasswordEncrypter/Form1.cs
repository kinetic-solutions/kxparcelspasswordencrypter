﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KxParcelsPasswordEncrypter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private string _sharedSecret = "4Jks1ksQeOsd661i8Dm";

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            string salt = Guid.NewGuid().ToString().Replace("-", String.Empty);
            txtEncPassword.Text = Encrypt(txtPassword.Text, salt);
            txtSalt.Text = salt;
        }
        public string Encrypt(string plainText, string salt)
        {
            byte[] saltarray = Encoding.ASCII.GetBytes(salt);

            string outStr = null;
            RijndaelManaged aesAlg = null;

            try
            {
                var key = new Rfc2898DeriveBytes(_sharedSecret, saltarray);

                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (var memoryStream = new MemoryStream())
                {
                    memoryStream.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    memoryStream.Write(aesAlg.IV, 0, aesAlg.IV.Length);

                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (var streamWriter = new StreamWriter(cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }
                    }

                    outStr = Convert.ToBase64String(memoryStream.ToArray());
                }
            }
            finally
            {
                if (aesAlg != null)
                {
                    aesAlg.Clear();
                }
            }

            return outStr;
        }
    }
}
